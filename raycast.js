const TILE_SIZE = 64;
const MAP_NUM_ROWS = 11;
const MAP_NUM_COLS = 15;

const WINDOW_WIDTH = MAP_NUM_COLS * TILE_SIZE;
const WINDOW_HEIGHT = MAP_NUM_ROWS * TILE_SIZE;

const FOV_ANGLE = deg2radius(60);

const WALL_STRIP_WIDTH = 2;
const NUM_RAYS = WINDOW_WIDTH / WALL_STRIP_WIDTH;

const MINIMAP_SCALE_FACTOR = 0.2;


class Map {
  constructor() {
    this.grid = [
      [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1],
      [1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1],
      [1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
      [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ];
  }
  render() {
    for (var i = 0; i < MAP_NUM_ROWS; i++) {
      for (var j = 0; j < MAP_NUM_COLS; j++) {
        var tileX = j * TILE_SIZE; 
        var tileY = i * TILE_SIZE;
        var tileColor = this.grid[i][j] == 1 ? "#222" : "#fff";
        stroke("#222");
        fill(tileColor);
        rect(
          MINIMAP_SCALE_FACTOR * tileX, 
          MINIMAP_SCALE_FACTOR * tileY, 
          MINIMAP_SCALE_FACTOR * TILE_SIZE, 
          MINIMAP_SCALE_FACTOR * TILE_SIZE);
      }
    }
  }
  hasWallAt(x, y) {
    return this.grid[Math.trunc(y / TILE_SIZE)][Math.trunc(x / TILE_SIZE)] != 0;
  }
}

class Player {
  constructor(grid) {
    this.grid = grid;
    this.x = WINDOW_WIDTH / 2;
    this.y = WINDOW_HEIGHT / 2;
    this. radius = 3;
    this.turnDirection = 0; // -1 left +1 right
    this.walkDirection = 0; // -1 back +1 front
    this.rotationAngle = Math.PI / 2;
    this.moveSpeed = 2.0;
    this.rotationSpeed = deg2radius(2);
    this.rays = [];
  }

  update() {
    var moveStep = this.walkDirection * this.moveSpeed;
    // calculate new position
    var nextStepX = this.x + Math.cos(player.rotationAngle) * moveStep;
    var nextStepY = this.y + Math.sin(player.rotationAngle) * moveStep;

    if (!grid.hasWallAt(nextStepX, nextStepY)) {

      this.x = nextStepX;
      this.y = nextStepY;
  
      this.castAllRays();
    }

    this.rotationAngle += this.turnDirection * this.rotationSpeed;

  }

  castAllRays() {  
    // start fisrt ray sustracting half of the FOV
    var angle = this.rotationAngle - (FOV_ANGLE / 2);
    
    this.rays = [];
    for (var i = 0; i < NUM_RAYS; i++) {
      var ray = this.castRay(angle + i * (FOV_ANGLE / NUM_RAYS));
      this.rays.push(ray);
    }
  }

  castRay(a) {
    var angle = normalizeAngle(a);
    var xintercept, yintercept;
    var xstep, ystep;
    var isRayFacingDown = angle < Math.PI;
    var isRayFacingRight = angle < 0.5 * Math.PI || angle > 1.5 * Math.PI;

    ///////////////////////////////////////
    // Horizontal ray grid intersection
    ///////////////////////////////////////
    var nextTouchX = 0;
    var nextTouchY = 0;
    var horDistance = Number.MAX_VALUE

    // Find the y-intercept of the closest horizontal grid intersection
    yintercept = Math.floor(this.y / TILE_SIZE) * TILE_SIZE;
    yintercept += isRayFacingDown ? TILE_SIZE : 0;
    
    // Find the x-intercept of the closest horizontal grid intesection
    xintercept = this.x + (yintercept - this.y) / Math.tan(angle);

    // Calculate the increment
    ystep = TILE_SIZE;
    ystep *= isRayFacingDown ? 1 : -1;

    xstep = TILE_SIZE / Math.tan(angle);
    xstep *= (!isRayFacingRight && xstep > 0) ? -1 : 1;
    xstep *= (isRayFacingRight && xstep < 0) ? -1 : 1;

    var nextTouchX = xintercept;
    var nextTouchY = yintercept;


    while(nextTouchX >= 0 && nextTouchX <= WINDOW_WIDTH && nextTouchY >=0 && nextTouchY <= WINDOW_HEIGHT) {
      if (this.grid.hasWallAt(nextTouchX, nextTouchY - (isRayFacingDown ? 0 : 1))) {
        // We found a wall hit
        horDistance = Math.sqrt((this.y - nextTouchY) * (this.y - nextTouchY) + (this.x - nextTouchX) * (this.x - nextTouchX));
        break;
      } else {
        nextTouchX += xstep;
        nextTouchY += ystep;
      }
    }

    ///////////////////////////////////////
    // Vertical ray grid intersection
    ///////////////////////////////////////
    var vertDistance = Number.MAX_VALUE;
    nextTouchX = 0;
    nextTouchY = 0;

    // Find the x-intercept of the closest vertical grid intersection
    xintercept = Math.floor(this.x / TILE_SIZE) * TILE_SIZE;
    xintercept += isRayFacingRight ? TILE_SIZE : 0;
    
    // Find the y-intercept of the closest vertical grid intesection
    yintercept = this.y + (xintercept - this.x) * Math.tan(angle);

    // Calculate the increment
    xstep = TILE_SIZE;
    xstep *= isRayFacingRight ? 1 : -1;

    ystep = TILE_SIZE * Math.tan(angle);
    ystep *= (!isRayFacingDown && ystep > 0) ? -1 : 1;
    ystep *= (isRayFacingDown && ystep < 0) ? -1 : 1;

    var nextTouchX = xintercept;
    var nextTouchY = yintercept;

    while(nextTouchX >= 0 && nextTouchX <= WINDOW_WIDTH && nextTouchY >=0 && nextTouchY <= WINDOW_HEIGHT) {
      if (this.grid.hasWallAt(nextTouchX - (isRayFacingRight ? 0 : 1), nextTouchY)) {
        // We found a wall hit
        vertDistance = Math.sqrt((this.y - nextTouchY) * (this.y - nextTouchY) + (this.x - nextTouchX) * (this.x - nextTouchX));
        break;
      } else {
        nextTouchX += xstep;
        nextTouchY += ystep;
      }
    }

    var distance = Math.min(vertDistance, horDistance);
    // direction
    var direction = (distance == vertDistance) ? 1 : 0;

    return new Ray(angle, distance, direction);
  }

  render() {

    /////////////////////////////////
    // 3D PROJECTION
    /////////////////////////////////
    const distanceProjPlane = (WINDOW_WIDTH / 2) / Math.tan(FOV_ANGLE / 2);

    for(var colId = 0; colId < this.rays.length; colId++) {
      var ray = this.rays[colId];
      var correctDistance = ray.distance * Math.cos(this.rotationAngle - ray.angle);
      var wallStripHeight = (TILE_SIZE / correctDistance) * distanceProjPlane;
      // compute transparency depend on correct distance
      var alpha = 150 / correctDistance;

      var color = ray.wasHitVertical() ? 255 : 150; 

      fill(`rgba(${color}, ${color}, ${color}, ${alpha})`);
      noStroke();
      rect(
        colId * WALL_STRIP_WIDTH,
        (WINDOW_HEIGHT / 2) - (wallStripHeight / 2),
        WALL_STRIP_WIDTH,
        wallStripHeight
      )
    }

    /////////////////////////////////
    // MINIMAP
    /////////////////////////////////

    // First the plan
    this.grid.render()

    // All the rays
    for (var ray of this.rays) {
      ray.render(this.x, this.y);
    }

    // Second the player
    fill("red");
    circle(
      MINIMAP_SCALE_FACTOR * this.x, 
      MINIMAP_SCALE_FACTOR * this.y, 
      MINIMAP_SCALE_FACTOR * this.radius
    );
    stroke("black");
    line(
      MINIMAP_SCALE_FACTOR * this.x,
      MINIMAP_SCALE_FACTOR * this.y,
      MINIMAP_SCALE_FACTOR * (this.x + Math.cos(this.rotationAngle) * 15),
      MINIMAP_SCALE_FACTOR * (this.y + Math.sin(this.rotationAngle) * 15)
    );
  }

}

class Ray {
  constructor(angle, distance, direction) {
    this.angle = normalizeAngle(angle);
    this.distance = distance;
    this.direction = direction;
  }

  wasHitVertical() {
    return this.direction == 1;
  }

  render(x, y) {
    stroke("rgba(255,0,0,0.3)");
    line(
      MINIMAP_SCALE_FACTOR * x, 
      MINIMAP_SCALE_FACTOR * y, 
      MINIMAP_SCALE_FACTOR * (x + Math.cos(this.angle) * this.distance),
      MINIMAP_SCALE_FACTOR * (y + Math.sin(this.angle) * this.distance)
    );
  }
}

var grid = new Map();
var player = new Player(grid);

/////////////////////////////////////
// Angle functions
/////////////////////////////////////
function normalizeAngle(angle) {
  a = angle % (2 * Math.PI);
  if (a < 0) {
    return a + (2 * Math.PI);
  }
  return a;

}

function deg2radius(angle) {
  return angle * (Math.PI / 180)
}

////////////////////////////////
// Events function : Manage behaviour
////////////////////////////////
function keyPressed() {

  switch (keyCode) {
    case UP_ARROW:
      player.walkDirection = 1;
      break;
    case DOWN_ARROW:
      player.walkDirection = -1;
      break;
    case RIGHT_ARROW:
      player.turnDirection = 1;
      break;
    case LEFT_ARROW:
      player.turnDirection = -1;
      break;    
  }
}

function keyReleased() {

  switch (keyCode) {
    case UP_ARROW:
      player.walkDirection = 0;
      break;
    case DOWN_ARROW:
      player.walkDirection = 0;
      break;
    case RIGHT_ARROW:
      player.turnDirection = 0;
      break;
    case LEFT_ARROW:
      player.turnDirection = 0;
      break;    
  }
}

function setup() {
  createCanvas(WINDOW_WIDTH, WINDOW_HEIGHT);
}

function update() {
  player.update();
}

function draw() {
  clear("#212121");
  update();
  player.render();
}
